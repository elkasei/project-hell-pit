﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class healthCount : MonoBehaviour
{
    public int Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
        }
    }

    private int health = 100;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Spikes")
        {
            Health -= 25;
        }

        if (col.gameObject.tag == "Sinner")
        {
            Health -= 50;
        }

        if (col.gameObject.tag == "Devil")
        {
            Health -= 100;
        }

        if (Health <= 0)
        {
            SceneManager.LoadScene("EndGame");
        }

    }
}
